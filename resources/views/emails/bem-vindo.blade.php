@extends('emails.email-base')

@section('content')
    <tr>
        <td style="padding:0 40px;">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card card-default">
                            <div class="card-header">Bem vindo {{$user->name}} a equipe AdvSys!</div>

                            <div class="card-body">
                                Olá {{$user->name}}<br>
                                Agora você pode ficar ligado em todos os processos relacionados à você!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </td>
    </tr>
@endsection
