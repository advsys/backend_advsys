<?php

namespace App\Http\Requests;

use App\Models\Admin;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;
use Illuminate\Validation\Rule;

class AdminUpdateForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user() instanceof Admin && auth()->user()->is_master;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /** @var Route $route */
        $route = $this->getRouteResolver()();
        $admin = $route->parameter('admin');

        return [
            'name' => [
                'string',
                'min:3'
            ],
            'email' => [
                'email',
               Rule::unique('admins', 'email')->whereNot('email', $admin->email)
            ],
            'password' => [
                'min:6',
                'confirmed'
            ],
        ];
    }
}
