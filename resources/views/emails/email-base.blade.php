<!doctype html>

<html>

<head>

    <meta charset="utf-8"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">

    <style type="text/css">

        * {
            margin: 0px;
            padding: 0px;
        }

        table {
            width: 50%;
        }

        @media all and ( max-width: 768px ) {

            table {
                width: 100%;
            }

        }

    </style>

</head>

<body style="background:#37474f; width:100%; height:100%; font-family: 'Roboto', sans-serif; color:#37474f;">

<!-- Content Start Here -->

<div style="padding:10px;">

    <table style="background:#fff; text-align:left; margin:0px auto;">

        <thead>

        <tr>
            <th style="padding:40px 40px 0px 40px;">
                <img src="https://image.ibb.co/k66dC0/logo.png"
                     style="width:200px;"/>
            </th>
        </tr>

        </thead>

        <tbody>
        @yield('content')
        <tr>
            <td style="padding:0px 40px 40px 40px;">
                <p style="margin:0px; font-size:16px; margin-bottom:10px;">
                    Atenciosamente, <br />
                    Equipe <strong>AdvSys</strong>
                </p>
            </td>
        </tr>
        </tbody>

    </table>

</div>

<!-- Content End Here -->

</body>

</html>
