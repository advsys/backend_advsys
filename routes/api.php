<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'User'], function(){
    Route::post('login', 'UserController@login');
    Route::post('register', 'UserController@register');

    Route::group(['middleware' => 'jwt:user'], function(){
        Route::get('process', 'ProcessController@index');
    });
});

Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function(){
    Route::post('login', 'AdminController@login');

    Route::group(['middleware' => 'jwt:admin'], function(){
        Route::get('me', 'AdminController@me');

        Route::get('user', 'UserController@index');

        Route::get('user/{user}', 'UserController@show');

        Route::apiResource('admin', 'AdminController');

        Route::apiResource('process', 'ProcessController');

        Route::apiResource('event', 'EventController');
    });
});
