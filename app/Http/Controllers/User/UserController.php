<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserLoginForm;
use App\Http\Requests\UserRegisterForm;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    public function login(UserLoginForm $request)
    {
        $user = User::whereEmail($request['email'])->first();

        if(!Hash::check($request['password'], $user->password)){
            throw ValidationException::withMessages(['password' => ['Senha incorreta']]);
        }


        return new UserResource($user, true);
    }

    public function register(UserRegisterForm $request)
    {
        $user = User::create($request->toArray());

        Mail::send('emails.bem-vindo', ['user' => $user], function(Message $message) use ($user) {
            $message->to($user->email);
            $message->subject('Bem Vindo à AdvSys');
            $message->from('contato@advsys.com.br', 'AdvSys');
        });

        return new UserResource($user, true);
    }

    public function me()
    {
        return new UserResource(auth()->user());
    }
}
