<?php

use Illuminate\Database\Seeder;

class AdminSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Admin::create([
           'name' => 'Admin',
           'email' => 'admin@admin.com',
           'password' => '123456',
           'is_master' => true
        ]);
    }
}
