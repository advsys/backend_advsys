<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserUpdateForm;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $query  = User::query();

        $query->with(['processes']);

        if($search = $request->get('search')){
            $query->where('name', 'like', '%'.$search.'%');
        }

        $limit = $request->get('limit', 15);

        $query->orderBy(
            $request->get('sort', 'name'),
            $request->get('order', 'asc')
        );

        $results = $limit > 0 ? $query->paginate($limit) : $query->get();

        return UserResource::collection($results);
    }


    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return UserResource
     */
    public function show(User $user)
    {
        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserUpdateForm $request
     * @param User $user
     * @return UserResource
     */
    public function update(UserUpdateForm $request, User $user)
    {
        $user->update($request->all());

        return new UserResource($user);
    }
}
