<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception $exception
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if($request->isJson() || $request->expectsJson()) {
            return $this->makeJsonResponse($request, $exception);
        }
        return parent::render($request, $exception);
    }

    private function makeJsonResponse(Request $request, Exception $exception)
    {
        if($exception instanceof ValidationException) {
            return response()->json([
                'message' => $exception->validator->errors()->first(),
                'data' => $exception->validator->errors()->toArray(),
            ], Response::HTTP_BAD_REQUEST);
        }

        if($exception instanceof ModelNotFoundException) {
            return response()->json([
                'message' => 'Recurso não encontrado',
                'data' => [],
            ], Response::HTTP_NOT_FOUND);
        }

        if($exception instanceof RouteNotFoundException || $exception instanceof NotFoundHttpException || $exception instanceof MethodNotAllowedHttpException ) {
            return response()->json([
                'message' => 'Rota não encontrada',
                'data' => [
                    'url' => $request->getUri(),
                    'method' => $request->getMethod(),
                ],
            ], Response::HTTP_NOT_FOUND);
        }

        if($exception instanceof UnauthorizedHttpException || $exception instanceof TokenBlacklistedException) {
            return response()->json([
                'message' => 'Sessão expirada',
                'data' => [],
            ], Response::HTTP_UNAUTHORIZED, ['WWW-Authenticate' => 'jwt-auth']);
        }
        return response()->json([
            'message' => 'Erro Desconhecido',
            'data' => [
                'error' => $exception->getMessage()
            ],
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
