<?php

namespace App\Http\Resources;

use App\Models\Admin;
use Illuminate\Http\Resources\Json\JsonResource;

class AdminResource extends JsonResource
{
    public function __construct($resource, $withToken = false)
    {
        parent::__construct($resource);

        if($withToken && $resource instanceof Admin){
            $this->with = ['token' =>  \Tymon\JWTAuth\Facades\JWTAuth::fromUser($resource)];
        }
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = $this->resource->toArray();

        return $data;
    }
}
