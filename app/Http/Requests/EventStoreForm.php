<?php

namespace App\Http\Requests;

use App\Models\Admin;
use Illuminate\Foundation\Http\FormRequest;

class EventStoreForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user() instanceof Admin;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'min:3',
                'string'
            ],
            'begin_date' => [
                'required',
                'date_format:Y-m-d'
            ],
            'end_date' => [
                'required',
                'date_format:Y-m-d'
            ],
            'description' => [
                'required',
                'string'
            ],
        ];
    }
}
