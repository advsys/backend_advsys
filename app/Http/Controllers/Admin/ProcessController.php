<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProcessStoreForm;
use App\Http\Requests\ProcessUpdateForm;
use App\Http\Resources\ProcessResource;
use App\Models\Process;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;

class ProcessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $query  = Process::query();

        $query->with(['user']);

        $query->where('admin_id', auth()->id());

        if($search = $request->get('search')){
            $query->where('name', 'like', '%'.$search.'%');
        }

        $limit = $request->get('limit', 15);

        $query->orderBy(
            $request->get('sort', 'name'),
            $request->get('order', 'asc')
        );

        $results = $limit > 0 ? $query->paginate($limit) : $query->get();

        return ProcessResource::collection($results);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProcessStoreForm $request
     * @return ProcessResource
     */
    public function store(ProcessStoreForm $request)
    {
        $request['admin_id'] = auth()->id();

        $process = Process::create($request->all());

        Mail::send('emails.novo-processo', ['user' => $process->user], function(Message $message) use ($process) {
            $message->to($process->user->email);
            $message->subject('Novo Processo - AdvSys');
            $message->from('contato@advsys.com.br', 'AdvSys');
        });

        return new ProcessResource($process);
    }

    /**
     * Display the specified resource.
     *
     * @param Process $process
     * @return ProcessResource
     */
    public function show(Process $process)
    {
        return new ProcessResource($process);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProcessUpdateForm $request
     * @param Process $process
     * @return ProcessResource
     */
    public function update(ProcessUpdateForm $request, Process $process)
    {
        $process->update($request->all());

        return new ProcessResource($process);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Process $process
     * @return ProcessResource
     * @throws \Exception
     */
    public function destroy(Process $process)
    {
        $process->delete();

        return new ProcessResource($process);
    }
}
