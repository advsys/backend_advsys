<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\EventStoreForm;
use App\Http\Requests\EventUpdateForm;
use App\Http\Resources\EventResource;
use App\Models\Event;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $query  = Event::query();

        if($search = $request->get('search')){
            $query->where('name', 'like', '%'.$search.'%');
        }

        $limit = $request->get('limit', 15);

        $query->orderBy(
            $request->get('sort', 'name'),
            $request->get('order', 'asc')
        );

        $results = $limit > 0 ? $query->paginate($limit) : $query->get();

        return EventResource::collection($results);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param EventStoreForm $request
     * @return EventResource
     */
    public function store(EventStoreForm $request)
    {
        $event = Event::create($request->all());

        return new EventResource($event);
    }

    /**
     * Display the specified resource.
     *
     * @param Event $event
     * @return EventResource
     */
    public function show(Event $event)
    {
        return new EventResource($event);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EventUpdateForm $request
     * @param Event $event
     * @return EventResource
     */
    public function update(EventUpdateForm $request, Event $event)
    {
        $event->update($request->all());

        return new EventResource($event);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Event $event
     * @return EventResource
     * @throws \Exception
     */
    public function destroy(Event $event)
    {
        $event->delete();

        return new EventResource($event);
    }
}
