<?php

namespace App\Http\Resources;

use App\Models\Process;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    public function __construct($resource, $withToken = false)
    {
        parent::__construct($resource);

        if($withToken && $resource instanceof User){
            $this->with = ['token' =>  \Tymon\JWTAuth\Facades\JWTAuth::fromUser($resource)];
        }
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = $this->resource->toArray();

        $data['processes'] = $this->resource->processes->map(function(Process $process){
            return $process->toArray();
        });

        return $data;
    }
}
