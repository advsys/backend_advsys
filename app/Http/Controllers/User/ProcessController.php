<?php

namespace App\Http\Controllers\User;

use App\Exceptions\ProcessForbiddenException;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProcessResource;
use App\Models\Process;
use Illuminate\Http\Request;

class ProcessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $query  = Process::query();

        $query->where('user_id', auth()->id());

        if($search = $request->get('search')){
            $query->where('name', 'like', '%'.$search.'%');
        }

        $limit = $request->get('limit', 15);

        $query->orderBy(
            $request->get('sort', 'name'),
            $request->get('order', 'asc')
        );

        $results = $limit > 0 ? $query->paginate($limit) : $query->get();

        return ProcessResource::collection($results);
    }

    /**
     * Display the specified resource.
     *
     * @param Process $process
     * @return ProcessResource
     * @throws ProcessForbiddenException
     */
    public function show(Process $process)
    {
        if($process->user_id != auth()->id()){
            throw new ProcessForbiddenException();
        }

        return new ProcessResource($process);
    }

}
