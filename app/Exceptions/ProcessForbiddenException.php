<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

class ProcessForbiddenException extends Exception
{
    public function __construct()
    {
        parent::__construct('Este processo não pertence a você', Response::HTTP_FORBIDDEN);
    }
}
