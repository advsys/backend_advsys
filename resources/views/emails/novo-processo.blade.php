@extends('emails.email-base')

@section('content')
    <tr>
        <td style="padding:0 40px;">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card card-default">
                            <div class="card-header">Há um novo processo!</div>

                            <div class="card-body">
                                Olá {{$user->name}}<br>
                                Há um novo processo para você, entre no aplicativo e confira!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </td>
    </tr>
@endsection
