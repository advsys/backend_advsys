<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminLoginForm;
use App\Http\Requests\AdminStoreForm;
use App\Http\Requests\AdminUpdateForm;
use App\Http\Resources\AdminResource;
use App\Models\Admin;
use App\Models\Process;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class AdminController extends Controller
{
    public function login(AdminLoginForm $request)
    {
        $user = Admin::whereEmail($request['email'])->first();

        if(!Hash::check($request['password'], $user->password)){
            throw ValidationException::withMessages(['password' => ['Senha incorreta']]);
        }


        return new AdminResource($user, true);
    }

    public function me()
    {
        return new AdminResource(auth()->user());
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $query  = Admin::query();

        if($search = $request->get('search')){
            $query->where('name', 'like', '%'.$search.'%');
        }

        $limit = $request->get('limit', 15);

        $query->orderBy(
            $request->get('sort', 'name'),
            $request->get('order', 'asc')
        );

        $results = $limit > 0 ? $query->paginate($limit) : $query->get();

        return AdminResource::collection($results);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AdminStoreForm $request
     * @return AdminResource
     */
    public function store(AdminStoreForm $request)
    {
        $admin = Admin::create($request->all());

        return new AdminResource($admin);
    }

    /**
     * Display the specified resource.
     *
     * @param Admin $admin
     * @return AdminResource
     */
    public function show(Admin $admin)
    {
        return new AdminResource($admin);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AdminUpdateForm $request
     * @param Admin $admin
     * @return AdminResource
     */
    public function update(AdminUpdateForm $request, Admin $admin)
    {
        $admin->update($request->all());

        return new AdminResource($admin);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Admin $admin
     * @return AdminResource
     * @throws \Exception
     */
    public function destroy(Admin $admin)
    {
        $admin->processes()->each(function(Process $process){
           $process->update(['admin_id' => auth()->id()]);
        });

        $admin->delete();

        return new AdminResource($admin);
    }

}
